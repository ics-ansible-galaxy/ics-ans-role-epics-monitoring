ics-ans-role-epics-monitoring
=============================

Ansible role to install scripts to monitor EPICS modules:
- delete any new epics module release not created by jenkins
- ensure all new directories in EPICS modules are group writable

Requirements
------------

- ansible >= 2.7
- molecule >= 2.19

Role Variables
--------------

```yaml
epics_monitoring_modules_dir: /export/epics/modules
epics_monitoring_jenkins_user: srv_icsjenkins
```

For In-Kind EEE server, `epics_monitoring_jenkins_user` shall be set to "ess".

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-epics-monitoring
```

License
-------

BSD 2-clause
