import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_monitoring_services_running_and_enabled(host):
    for item in ('delete-non-jenkins-epics-release.service', 'epics-gid-write.service'):
        service = host.service(item)
        assert service.is_running
        assert service.is_enabled
